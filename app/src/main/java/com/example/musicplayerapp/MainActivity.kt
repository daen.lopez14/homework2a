package com.example.musicplayerapp

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.btn_details
import kotlinx.android.synthetic.main.activity_main.btn_next
import kotlinx.android.synthetic.main.activity_main.btn_play
import kotlinx.android.synthetic.main.activity_main.btn_previous
import kotlinx.android.synthetic.main.activity_main.seekbar


class MainActivity : AppCompatActivity() {

    private var mediaPLayer = MediaPlayer()
    lateinit var runnable: Runnable
    private var handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //--------------------------------------------------------
        //CREATING MEDIAPLAYER OBJECT ******************************
        mediaPLayer = MediaPlayer.create(this, R.raw.kriptonite)
        //adding the seek bar event, first set min and max values
        seekbar.max = mediaPLayer.duration
        seekbar.progress = 0
        //NAVIGATE BETWEEN ACITIVTIES***************************
        btn_details.setOnClickListener {
            Intent(this, detailsActivity::class.java).also {
                startActivity(it)
            }
        }
        //BOTON PARA DARLE PLAY************************************
        //btn_play.findViewById<View>(R.id.btn_play) as ImageButton
        //var btnPlay : ImageButton = findViewById(R.id.btn_play)
        //btnPlay.setOnClickListener { }

        btn_play.setOnClickListener {
            //check media player is not playing
            if (!mediaPLayer.isPlaying) {
                mediaPLayer.start()
                btn_play.setImageResource(R.drawable.ic_baseline_pause_24)
            } else {
                //the media player is playing and we can pause it
                mediaPLayer.pause()
                btn_play.setImageResource(R.drawable.ic_baseline_play_arrow_24)
            }
        }
        //NEXT TRACK BUTTON
        btn_next.setOnClickListener {
            mediaPLayer.stop()
            if (!mediaPLayer.isPlaying) {
                mediaPLayer = MediaPlayer.create(this, R.raw.withyou)
            } else {
                //the media player is playing and we can pause it
                mediaPLayer = MediaPlayer.create(this, R.raw.withyou)
                mediaPLayer.start()
            }
        }
        //BOTON PARA ANTERIOR CANCION
        btn_previous.setOnClickListener {
            mediaPLayer.stop()
            if (!mediaPLayer.isPlaying) {
                mediaPLayer = MediaPlayer.create(this, R.raw.kriptonite)
            } else {
                //the media player is playing and we can pause it
                mediaPLayer = MediaPlayer.create(this, R.raw.kriptonite)
                mediaPLayer.start()
            }
        }

        //ADJUSTING SEEKBAR***************************************
        //adding the seekbar event. when we change our seekbar progress the song will change the position
        seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekbar: SeekBar?, pos: Int, changed: Boolean) {
                //now changing the position of the seekbar will change music to that position
                if (changed) {
                    mediaPLayer.seekTo(pos)
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}
            override fun onStopTrackingTouch(p0: SeekBar?) {}
        })

        //---------------------------------------------------
        //changing seekbar position while the song is playing
        //we need to create a runnable object and a handler

        runnable = Runnable {
            seekbar.progress = mediaPLayer.currentPosition
            handler.postDelayed(runnable, ON_DELAYED_NUMBER)
        }
        handler.postDelayed(runnable, ON_DELAYED_NUMBER)

        //-----------------------------------------------------
        //when music finish to play, seekbar return to 0 and button switches image resource
        mediaPLayer.setOnCompletionListener {
            btn_play.setImageResource(R.drawable.ic_baseline_play_arrow_24)
            mediaPLayer.seekTo(0)
            seekbar.progress = 0
            mediaPLayer.stop()
            mediaPLayer.prepare()
            mediaPLayer.setOnPreparedListener() {
                Toast.makeText(this, "preparado", Toast.LENGTH_SHORT).show()
            }
        }

    }

    override fun onStart() {
        super.onStart()
        Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show()
    }

    override fun onStop() {
        super.onStop()
        Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show()
    }

    override fun onRestart() {
        super.onRestart()
        Toast.makeText(this, "onRestart", Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        releaseMediaPLayer()
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show()
    }

    private fun releaseMediaPLayer() {
        mediaPLayer.release()
    }

    companion object{
        const val ON_DELAYED_NUMBER = 1000L
    }
}
